 <?php
require_once('./include/cache_start.php');
require_once('./include/db_info.inc.php');
require_once('./include/const.inc.php');
require_once('./include/setlang.php');
if (!isset($_SESSION['user_id'])){

	$view_errors= "<a href=loginpage.php>Please Login First</a>";
	require("template/".$OJ_TEMPLATE."/error.php");
	exit(0);
}

require("template/".$OJ_TEMPLATE."/addworkbook.php");
?>
