<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $view_title?></title>
	<link rel=stylesheet href='./template/<?php echo $OJ_TEMPLATE?>/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
</head>
<body>
<div id="wrapper">
	<?php require_once("oj-header.php");?>
	<div id=main>
	<h3>문제집</h3>
	<a href="./addworkbook">문제집 추가</a>
	<hr class="featurette-divider">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<td width='5%'>ID</td>
				<td width='78%'>제목</td>
				<td width='10%'>제작자</td>
				<td width='7%'></td>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($view_workbook as $row){
					echo "<tr>";
					foreach($row as $content){
						echo "<td>";
						echo $content;
						echo "</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
		<div id=foot>
		<?php require_once("oj-footer.php");?>
		</div>
	</div>
</div>
</body>
</html>