<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $view_title?></title>
	<link rel=stylesheet href='./template/<?php echo $OJ_TEMPLATE?>/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src='bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
     <link rel="next" href="submitpage.php?<?php
        
        if ($pr_flag){
		echo "id=$id";
	}else{
		echo "cid=$cid&pid=$pid&langmask=$langmask";
	}
        
        ?>">
</head>
<body>

<div id="wrapper">
	<?php
	if(isset($_GET['id']))
		require_once("oj-header.php");
	else
		require_once("contest-header.php");
	
	?>
<div id=main>
	
	<?php
	
	if ($pr_flag){
		echo "<title>$MSG_PROBLEM $row->problem_id. -- $row->title</title>";
		echo "<center><h2>$id: $row->title</h2>";
	}else{
		$PID="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		echo "<title>$MSG_PROBLEM $PID[$pid]: $row->title </title>";
		echo "<center><h2>$MSG_PROBLEM $PID[$pid]: $row->title</h2>";
	}

	echo "<span><strong>Time Limit : </strong></span><span class='label label-info'>$row->time_limit 초</span>&nbsp;&nbsp;";
	echo "<span><strong>Memory Limit : </strong></span><span class='label label-warning'>".$row->memory_limit."  MB</span>";
	if ($row->spj) echo "&nbsp;&nbsp;<span class='label label-danger'> Special Judge</span>";
	echo "<br><span>$MSG_SUBMIT: </span>".$row->submit."&nbsp;&nbsp;";
	echo "<span class>정답 : </span>".$row->accepted."&nbsp;&nbsp;"; 
	$a = $row->submit;
	$b = $row->accepted;
	$c = $b/$a*100;
	$c = round($c,2);
	echo "<span>정답률: </span>".$c."%<br><br>";
	if ($pr_flag){
		echo "<a class='buttontext' href='submitpage.php?id=$id'><button class='btn btn-primary'>$MSG_SUBMIT</button></a>";
	}else{
		echo "<a class='buttontext' href='submitpage.php?cid=$cid&pid=$pid&langmask=$langmask'><button class='btn btn-primary'>$MSG_SUBMIT</button></a>";
	}
	echo "<a class='buttontext' href='problemstatus.php?id=".$row->problem_id."'><button class='btn btn-primary'>$MSG_STATUS</button></a>";?>

		<div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    다음 문제집에 추가 <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <?php
		foreach($workbook_view as $content)
		{
			echo "<li><a href='#' onclick='wadd(";
			echo $content[0];
			echo ",";
			echo $id;
			echo ")'>";
			echo $content[1];
			echo "</a></li>";
		}
	?>
    <li class="divider"></li>
    <li><a href="workbook">문제집 보기</a></li>
  </ul>
</div>
<?php
	  if(isset($_SESSION['administrator'])){
      require_once("include/set_get_key.php");
      ?>
      [<a href="admin/problem_edit.php?id=<?php echo $id?>&getkey=<?php echo $_SESSION['getkey']?>" >Edit</a>]
      [<a href="admin/quixplorer/index.php?action=list&dir=<?php echo $row->problem_id?>&order=name&srt=yes" >TestData</a>]
      <?php

    }
	echo "<br>";
	$id = $_GET['id']; ?>

<?php
	echo "</center>";
	
	echo "<h2>$MSG_Description</h2><div><hr class='featurette-divider'>".$row->description."</div>";
	echo "<h2>$MSG_Input</h2><div><hr class='featurette-divider'>".$row->input."</div>";
	echo "<h2>$MSG_Output</h2><div><hr class='featurette-divider'>".$row->output."</div>";
	
  
	
	$sinput=str_replace("<","&lt;",$row->sample_input);
  $sinput=str_replace(">","&gt;",$sinput);
	$soutput=str_replace("<","&lt;",$row->sample_output);
  $soutput=str_replace(">","&gt;",$soutput);

	echo "<h2>$MSG_Sample_Input</h2>
			<hr class='featurette-divider'><pre class=content><span>".($sinput)."</span></pre>";
	echo "<h2>$MSG_Sample_Output</h2>
			<hr class='featurette-divider'><pre class=content><span>".($soutput)."</span></pre>";
	if ($pr_flag||true) 
		echo "<h2>$MSG_HINT</h2>
			<hr class='featurette-divider'><pre class=content><span>".($row->hint)."</span></pre>";
	if ($pr_flag) 
		echo "<h2>$MSG_Source</h2>
			<hr class='featurette-divider'><div class=content><p><a href='problemset.php?search=$row->source'>".nl2br($row->source)."</a></p></div>";
	echo "<center>";
	
	if(isset($_SESSION['administrator'])){
      require_once("include/set_get_key.php");
  ?>
     [<a href="admin/problem_edit.php?id=<?php echo $id?>&getkey=<?php echo $_SESSION['getkey']?>" >Edit</a>]
      [<a href="admin/quixplorer/index.php?action=list&dir=<?php echo $row->problem_id?>&order=name&srt=yes" >TestData</a>]
     <?php
  }	
  echo "</center>";
	?>
	<div id=disqusfoot>
		<?php require_once("disqusfooter.php");?>
		</div>
<div id=foot>
	<?php require_once("oj-footer.php");?>

</div><!--end foot-->
</div><!--end main-->
<script>
	function wadd(wid, pid)
	{
		$.ajax({
			type: 'post',
			url: 'addWproblem.php',
			data: {
				pid: pid,
				wid: wid
			},
			success: function(data){
				alert("추가하였습니다");
			}
		});
	}
</script>
</div><!--end wrapper-->
</body>
</html>
