<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $view_title?></title>
        <link rel=stylesheet href='./template/<?php echo $OJ_TEMPLATE?>/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
</head>
<body>
<div id="wrapper">
        <?php require_once("oj-header.php");?>
<div id=main>
<h2>Welcome to SCCC Online Judge</h2>
<hr class="featurette-divider">
<center>
	<div id="login-wrapper" style="width: 200px;">
	<form action=login.php method=post>
		<div class="input-group">
			<span class="input-group-addon">@</span>
			<input class="form-control" name="user_id" type="text" placeholder="User ID">
		</div>
		<br>
		<div class="input-group" >
			<span class="input-group-addon">@</span>
			<input class="form-control" name="password" type="password" placeholder="Password">
		</div>
		<br>
		<div class="button-group">
			<a href="lostpassword.php"><button class="btn btn-default" name="lost_password" type="button" style="text-align:center;">Lost Password</button></a>
			<button class="btn btn-default" name="submit" type="submit" style="text-align:center;">Log in!</button>
		</div>
	</form>
	</div>
</center>
<div id=foot>
        <?php require_once("oj-footer.php");?>

</div><!--end foot-->
</div><!--end main-->
</div><!--end wrapper-->
</body>
</html>

