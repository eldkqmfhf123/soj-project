<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $view_title?></title>
	<link rel=stylesheet href='./template/<?php echo $OJ_TEMPLATE?>/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
</head>
<body>
<div id="wrapper">
	<?php require_once("oj-header.php");?>
	<div id=main>
	<h3><?php echo $view_title ?></h3>
	<h5><?php echo $view_description ?></h5>
	<hr class="featurette-divider">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<td width='5%'></td>
				<td width='5%'>문제ID</td>
				<td width='60%'>문제명</td>
				<td width='10%'>정답</td>
				<td width='10%'>제출</td>
				<td width='10%'>정답률</td>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($view_problemset as $row){
					echo "<tr>";
					foreach($row as $content){
						echo "<td>";
						echo $content;
						echo "</td>";
					}
					echo "</tr>";
				}
			?>
		</tbody>
	</table>
		<div id=disqusfoot>
		<?php require_once("disqusfooter.php");?>
		</div>
		<div id=foot>
		<?php require_once("oj-footer.php");?>
		</div>
	</div>
</div>
</body>
</html>