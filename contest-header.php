<?php  
    require_once('./include/cache_start.php');

  
	require_once('./include/db_info.inc.php');

  if(isset($OJ_LANG)){
		require_once("./lang/$OJ_LANG.php");
	}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel=stylesheet href='./include/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
</head>
<?php if(isset($_GET['cid']))
	$cid=intval($_GET['cid']);
if (isset($_GET['pid']))
	$pid=intval($_GET['pid']);
?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="/SOJ/">SCCC Online Judge</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a class=hd href='./'><?php echo $MSG_HOME?></a></li>
				<li><a class=hd href='./contest.php?cid=<?php echo $cid?>'><?php echo $MSG_PROBLEMS?></a></li>
				<li><a class=hd href='./contestrank.php?cid=<?php echo $cid?>'><?php echo $MSG_STANDING?></a></li>
				<li><a class=hd href='./status.php?cid=<?php echo $cid?>'><?php echo $MSG_STATUS?></a></li>
				<li><a class=hd href='./conteststatistics.php?cid=<?php echo $cid?>'><?php echo $MSG_STATISTICS?></a></li>
			</ul>
			<ul class="nav navbar-nav pull-right">
				<script src="include/profile.php?<?php echo rand();?>"
				></script>
			</ul>
		</div>
	</div>
</nav>
<?php 
$view_marquee_msg=file_get_contents($OJ_SAE?"saestor://web/msg.txt":"./admin/msg.txt");
   
?>
<div id=broadcast>
<marquee id=broadcast scrollamount=1 direction=up scrolldelay=250 onMouseOver='this.stop()' onMouseOut='this.start()';>
  <?php echo $view_marquee_msg?>
</marquee>
</div><!--end broadcast-->
