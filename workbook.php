<?php
require_once('./include/cache_start.php');
require_once('./include/db_info.inc.php');
require_once('./include/my_func.inc.php');
require_once('./include/setlang.php');

$view_title="문제집";

if (isset($_GET['wid'])) {  // Workbook was selected
	$wid = intval($_GET['wid']);
	$view_wid = $wid;
	$sub_arr=Array();
	// submit
	if (isset($_SESSION['user_id'])){
	$sql="SELECT `problem_id` FROM `solution` WHERE `user_id`='".$_SESSION['user_id']."'".
																		   //  " AND `problem_id`>='$pstart'".
																		   // " AND `problem_id`<'$pend'".
		" group by `problem_id`";
	$result3=@mysql_query($sql) or die(mysql_error());
	while ($row=mysql_fetch_array($result3))
		$sub_arr[$row[0]]=true;
	}

	$acc_arr=Array();
	// ac
	if (isset($_SESSION['user_id'])){
	$sql="SELECT `problem_id` FROM `solution` WHERE `user_id`='".$_SESSION['user_id']."'".
																		   //  " AND `problem_id`>='$pstart'".
																		   //  " AND `problem_id`<'$pend'".
		" AND `result`=4".
		" group by `problem_id`";
	$result2=@mysql_query($sql) or die(mysql_error());
	while ($row=mysql_fetch_array($result2))
		$acc_arr[$row[0]]=true;
	}

	$sql = "SELECT * FROM `workbook` WHERE `workbook_id` = '$wid'";
	$result = mysql_query($sql);
	$result_cnt = mysql_num_rows($result);
	if($result_cnt == 0) // no exist workbook
	{
		mysql_free_result($result);	
		echo "No exist workbook!";
	}
	else
	{
		$row = mysql_fetch_object($result);
		$view_title = $row->title;
		$view_description = $row->description;
		$view_author = $row->author;
	}

	$sql = "SELECT * 
FROM (

SELECT  `problem`.`title` AS  `title` ,  `problem`.`problem_id` AS  `pid` , accepted AS accepted, submit AS submit, `spj` AS `spj`
FROM  `workbook_problem` ,  `problem` 
WHERE  `workbook_problem`.`problem_id` =  `problem`.`problem_id` 
AND  `problem`.`defunct` =  'N'
AND  `workbook_problem`.`workbook_id` =$wid
ORDER BY  `pid`
)problem";
	$result = mysql_query($sql);
	
	$cnt = 0;
	while($row = mysql_fetch_object($result)) {
		if (isset($sub_arr[$row->pid])){
			if (isset($acc_arr[$row->pid])) 
				$view_problemset[$cnt][0]="<div class='label label-success'>Y</div>";
			else 
				$view_problemset[$cnt][0]= "<div class='label label-danger'>N</div>";
		}else{
			$view_problemset[$cnt][0]= "<div class=none> </div>";
		}
		$view_problemset[$cnt][1] = $row->pid;
		if($row->spj)
			$view_problemset[$cnt][2] = "<a href='problem.php?id=$row->pid'>".$row->title."&nbsp; &nbsp;"."</a><span class='label label-info'>Special Judge</span>";
		else
			$view_problemset[$cnt][2] = "<a href='problem.php?id=$row->pid'>$row->title</a>";
		$view_problemset[$cnt][3] = $row->accepted;
		$view_problemset[$cnt][4] = $row->submit;
		$a = $row->submit;
		$b = $row->accepted;
		$c = $b/$a*100;
		$c = round($c);
		$view_problemset[$cnt][5] = "$c%";
		$cnt++;
	}
	mysql_free_result($result);
}
else // 선택안됨
{
	$sql = "SELECT * FROM `workbook` ORDER BY `workbook_id` DESC limit 100";
	$result = mysql_query($sql);

	$view_workbook = Array();
	$i = 0;
	while($row = mysql_fetch_object($result))
	{
		$view_workbook[$i][0] = $row->workbook_id; // workbook_id
		$view_workbook[$i][1] = "<a href='workbook.php?wid=$row->workbook_id'>$row->title</a>"; //title
		$view_workbook[$i][2] = "<a href='userinfo.php?user=$row->author'>$row->author</a>"; //author;
		$official = intval($row->official);
		if( $official == 0 ) // unofficial
		{
			$view_workbook[$i][3] = "<span class='label label-warning'>unofficial</span>";
		}
		else //official
		{
			$view_workbook[$i][4] = "<span class='label label-info'>official</span>";
		}
		$i++;
	}
	mysql_free_result($result);
}


/////////////////////////Template
if (isset($_GET['wid']))
    require("template/" . $OJ_TEMPLATE . "/workbook.php");
else
    require("template/" . $OJ_TEMPLATE . "/workbookset.php");
/////////////////////////Common foot
if (file_exists('./include/cache_end.php'))
    require_once('./include/cache_end.php');
?>
 